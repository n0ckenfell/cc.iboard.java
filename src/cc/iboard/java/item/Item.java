package cc.iboard.java.item;

public class Item {

	private long id = 0;
	private String subject = null;
	private String body = null;

	public long getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getBody() {
		return this.body;
	}

}
