package cc.iboard.java.board;

public class BoardRepository {

	public BoardRepository() {}

	public Board find(long id) {
		return BoardLoader.find(id);
	}
	
}
