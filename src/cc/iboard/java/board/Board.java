package cc.iboard.java.board;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;

import cc.iboard.java.Config;
import cc.iboard.java.topic.Topic;

public class Board {

	private String name = Config.NEW_BOARD_NAME;
	private long id = Config.NEW_ENTITY_ID;
	private final ArrayList<Topic> topics = new ArrayList<Topic>();

	public Board() {
	}

	@SuppressWarnings("rawtypes")
	public Board(HashMap jsonObject) {
		long _id = (long) jsonObject.get("id");
		String _name = (String) jsonObject.get("name");
		JSONArray _topics = (JSONArray) jsonObject.get("topics");

		setupAttributes(_id, _name, _topics);
	}

	private void setupAttributes(long _id, String _name, JSONArray _topics) {
		setName(_name);
		setId(_id);
		setupTopics(_topics);
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public long getId() {
		return this.id;
	}

	public ArrayList<Topic> getTopics() {
		return this.topics;
	}

	public Topic getTopic(int id) {
		Iterator<Topic> topics = getTopics().iterator();
		while (topics.hasNext()) {
			Topic next = topics.next();
			if (next.getId() == id) {
				return next;
			}
		}
		return null;
	}

	private void addTopic(Topic t) {
		this.topics.add(t);
	}

	private void setupTopics(JSONArray jsonTopics) {
		for (int i = 0; i < jsonTopics.size(); i++) {
			this.addTopic(new Topic(jsonTopics.get(i)));
		}
	}
}
