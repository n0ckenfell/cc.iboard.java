package cc.iboard.java.board;

import java.util.HashMap;

import org.json.simple.JSONValue;

public class BoardLoader {

	@SuppressWarnings("rawtypes")
	public static Board find(long _id) {
		HashMap hashMap = (HashMap) JSONValue.parse(BoardGateway.getJson());
		return new Board(hashMap);
	}

}
