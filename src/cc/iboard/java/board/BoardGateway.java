package cc.iboard.java.board;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;

import cc.iboard.java.Config;

public final class BoardGateway {

	public BoardGateway() {
	}

	public static final String getJson() {
		File f = new File(Config.FIXTURE_BOARD_JSON);
		try {
			byte[] bytes = Files.readAllBytes(f.toPath());
			return new String(bytes, "UTF-8");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

}
