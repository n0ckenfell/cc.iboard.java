package cc.iboard.java.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

import org.junit.Test;

import cc.iboard.java.item.Item;
import cc.iboard.java.topic.Topic;

public class TopicTest {

	@Test
	public void testTopic() {
		Topic t = new Topic();
		assertEquals(null, t.getSubject());
	}

	@Test
	public void testIdSetter() {
		Topic t = new Topic();
		t.setId(123);
		assertEquals(123, t.getId());
	}

	@Test
	public void testSubjectSetter() {
		Topic t = new Topic();
		t.setSubject("A New Topic");
		assertEquals("A New Topic", t.getSubject());
	}

	@Test
	public void testGetItems() {
		Topic t = new Topic();
		ArrayList<Item> emptyList = new ArrayList<Item>();
		assertEquals(emptyList, t.getItems());
	}

	@Test
	public void testAddItem() {
		Topic t = new Topic();
		Item i = new Item();
		i.setId(123);
		t.add(i);
		assertEquals(123, t.getItems().get(0).getId());
	}

}
