package cc.iboard.java.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BoardRepositoryTest.class, BoardTest.class, TopicTest.class, ItemTest.class })

public class AllTests {
}
