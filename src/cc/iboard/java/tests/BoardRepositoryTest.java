package cc.iboard.java.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import cc.iboard.java.board.Board;
import cc.iboard.java.board.BoardRepository;
import cc.iboard.java.topic.Topic;

public class BoardRepositoryTest {

	BoardRepository repo;
	Board firstBoard;

	@Before
	public void setUp() throws Exception {
		repo = new BoardRepository();
		firstBoard = repo.find(1);
	}

	@Test
	public void testFind() {
		assertEquals(1, firstBoard.getId());
		assertEquals("iBoard Production Resources", firstBoard.getName());
	}

	@Test
	public void testLoadWithTopics() {
		assertEquals(1, firstTopicOf(firstBoard).getId());
		assertEquals("Static Content", firstTopicOf(firstBoard).getSubject());
	}

	// Helpers

	private Topic firstTopicOf(Board firstBoard) {
		return firstBoard.getTopic(1);
	}

}
