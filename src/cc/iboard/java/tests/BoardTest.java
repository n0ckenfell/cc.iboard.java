package cc.iboard.java.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import cc.iboard.java.Config;
import cc.iboard.java.board.Board;

public class BoardTest {

	Board board;

	@Before
	public void setUp() {
		board = new Board();
	}

	@Test
	public void testBoardInitialization() {
		assertEquals(null, board.getName());
		assertEquals(Config.NEW_ENTITY_ID, board.getId());
	}

	@Test
	public void testSetName() {
		board.setName("Board One");
		assertEquals("Board One", board.getName());
	}

	@Test
	public void testSetId() {
		board.setId(123);
		assertEquals(123, board.getId());
	}

	@Test
	public void testTopicsIsArray() {
		assertTrue(board.getTopics() instanceof ArrayList<?>);
	}
}
