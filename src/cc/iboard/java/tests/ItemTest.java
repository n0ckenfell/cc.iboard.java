package cc.iboard.java.tests;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cc.iboard.java.item.Item;

public class ItemTest {

	Item item;

	@Before
	public void setUp() throws Exception {
		item = new Item();
		item.setId(1);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetId() {
		assertEquals(1, item.getId());
	}

	@Test
	public void testSetId() {
		item.setId(123);
		assertEquals(123, item.getId());
	}

	@Test
	public void testSubject() {
		item.setSubject("A new Subject");
		assertEquals("A new Subject", item.getSubject());
	}

	@Test
	public void testBody() {
		item.setBody("A new Body");
		assertEquals("A new Body", item.getBody());
	}

}
