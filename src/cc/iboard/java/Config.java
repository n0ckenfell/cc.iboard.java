package cc.iboard.java;

public final class Config {
	public static final long NEW_ENTITY_ID = 0;
	public static final String NEW_BOARD_NAME = null;
	public static final String FIXTURE_BOARD_JSON = "fixtures/board.json";

	private Config() {
	}
}
