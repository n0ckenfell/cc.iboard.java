package cc.iboard.java.topic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import cc.iboard.java.item.Item;

public class Topic {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

	private String subject;
	private long id;
	private final ArrayList<Item> items;

	public Topic() {
		items = new ArrayList<Item>();
	}

	public Topic(Object topic) {
		items = new ArrayList<Item>();
		setupFromJsonObject(topic);
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public void add(Item item) {
		getItems().add(item);
	}

	@SuppressWarnings("rawtypes")
	public void setupFromJsonObject(Object topicObj) {
		String subject = (String) ((HashMap) topicObj).get("subject");
		long id = (long) ((HashMap) topicObj).get("id");
		this.setSubject(subject);
		this.setId(id);
		LOGGER.log(Level.INFO, this.toString());

	}

	@Override
	public String toString() {
		return super.toString() + " Subject: " + this.getSubject() + ", Id: " + this.getId();
	}

}
